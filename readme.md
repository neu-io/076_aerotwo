###aka Orbeat  
  
The repo has custom packages, you'll need system git to resolve those.  
Unity 2018.3.7f - 9f would work.

Sounds matrix:  
  
| Element               | Sound Shape           | Helm Channel          | Helm Sound                       |
|:---------------------:|:---------------------:|:---------------------:|:--------------------------------:|  
|Drum                   |    DrumKit01          |                       | DK3_BD, DK3_HT, DK3_CH, DK4_CB   |  
|DrumKit02              |    DrumKit02          |                       | DK3_BD, DK1_CP, DK3_RS, DK4_SD   |  
|Bass                   |    SoundShape03       |       0               |    UG On and ON                  |  
|Bass2                  |    SoundShape05       |       5               |                                  |  
|Bass3                  |    SoundShape01       |       6               |                                  |  
|Lead                   |    SoundShape05       |       2               |   MT Easy Vibrato                |  
|Lead2                  |    SoundShape02       |       5               |                                  |  
|Lead3                  |    SoundShape07       |       6               |                                  |  

