﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MultipleSoundObjectSpawner : MonoBehaviour {


    public ControlServer controlServer;

    public SoundType[] Sounds = new SoundType[4];

    IEnumerator Start ()

    {

        controlServer.StartServer();
        while (WorldSettingsScript.instance.player == null) yield return null;

        for( int i = 0; i < Sounds.Length; i++ )
        {
            if( Sounds[i] == SoundType.Drum || Sounds[i] == SoundType.DrumKit2 )
            {
                for( int j = 0; j < 20; j++ )
                {
                    Spawn(Sounds[i]);
                }
            } else
            {
                for( int k = 0; k < 8; k++ )
                {
                    Spawn( Sounds[i] );
                }
            }
        }


    }

    void Spawn(SoundType type) {

      
        float angle = Random.Range(-Mathf.PI, Mathf.PI);
        float radius = Random.Range(.5f, 3f);
        float height = Random.Range(0f, 2f);
        //Debug.Log("Typ: " + type + " height: " + height);
        var position = new Vector3( Mathf.Sin (angle) * radius, height,  Mathf.Cos(angle) * radius);
        var player = WorldSettingsScript.instance.player;
        player.SpawnSound(type, position);


    }
}
