﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;

public class ARCoreAlignment : MonoBehaviour
{
    public static List<ARCoreMarker> _markers = new List<ARCoreMarker>();
    [Require] public ARAlignment alignment;
    private List<AugmentedImage> m_TempAugmentedImages = new List<AugmentedImage>();

    private void Update()
    {
        Session.GetTrackables<AugmentedImage>(m_TempAugmentedImages, TrackableQueryFilter.Updated);
        foreach (AugmentedImage tempImage in m_TempAugmentedImages)
        {
            MarkerDetected(tempImage);
        }
    }

    void MarkerDetected(AugmentedImage image)
    {
        GameObject imageObject = null;
        foreach (var marker in _markers)
        {
            if (image.Name == marker._imageName)
            {
                imageObject = marker.gameObject;
                break;
            }
        }
        if (imageObject == null)
            return;

        var position = image.CenterPose.position;
        var rotation = image.CenterPose.rotation;


        alignment.MarkerDetected(imageObject, position, rotation);
    }
}
