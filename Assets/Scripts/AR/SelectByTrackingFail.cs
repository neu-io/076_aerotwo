﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using GoogleARCore;

[RequireComponent(typeof(UISelector))]
public class SelectByTrackingFail : MonoBehaviour {
    LostTrackingReason failureReason;

    public UISelector failSelector;
    public GameObject none;
    public GameObject motion;
    public GameObject noFeatures;
    public GameObject noLight;
    public GameObject other;

    void Start() {
        if (failSelector == null) failSelector = GetComponent<UISelector>();
        UnityARSessionNativeInterface.ARFrameUpdatedEvent += UpdateARFrame;

    }
    void Update() {
        if (Session.LostTrackingReason != failureReason) {
            failureReason = Session.LostTrackingReason;
            FailStateSwitch(failureReason);
        }
    }
    void UpdateARFrame(UnityARCamera camera) {
        switch (camera.trackingReason) {
            case ARTrackingStateReason.ARTrackingStateReasonExcessiveMotion:
                Debug.Log("motion fail");
                failSelector.SetElement(motion);
                break;

            case ARTrackingStateReason.ARTrackingStateReasonInsufficientFeatures:
                Debug.Log("feature fail");
                failSelector.SetElement(noFeatures);
                break;


            case ARTrackingStateReason.ARTrackingStateReasonInitializing:
                Debug.Log("other fail");
                failSelector.SetElement(other);
                break;

            case ARTrackingStateReason.ARTrackingStateReasonNone:
                Debug.Log("tracking");
                failSelector.SetElement(none);
                break;

            default:
                Debug.Log("probably tracking");
                failSelector.SetElement(none);
                break;
        }
    }

    void FailStateSwitch(LostTrackingReason failReason) {
        switch (failReason) {
            case LostTrackingReason.ExcessiveMotion:
                Debug.Log("motion fail");
                failSelector.SetElement(motion);
                break;

            case LostTrackingReason.InsufficientFeatures:
                Debug.Log("feature fail");
                failSelector.SetElement(noFeatures);
                break;

            case LostTrackingReason.InsufficientLight:
                Debug.Log("light fail");
                failSelector.SetElement(noLight);
                break;

            case LostTrackingReason.BadState:
                Debug.Log("other fail");
                failSelector.SetElement(other);
                break;

            case LostTrackingReason.None:
                Debug.Log("tracking");
                failSelector.SetElement(none);
                break;

            default:
                Debug.Log("probably tracking");
                failSelector.SetElement(none);
                break;
        }
    }
}
