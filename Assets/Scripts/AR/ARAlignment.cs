using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ARAlignment : MonoBehaviour {
    [Require] public GameObject _root;

    bool _aligned;

    public delegate void OnAlignmentGot();
    public event OnAlignmentGot onAlignmentGot;

    public bool aligned {
        get { return _aligned; }
    }

    public void GetAlignment() {
        if (!_aligned) {
            _aligned = true;

            if (onAlignmentGot != null) onAlignmentGot();
        }
    }


    // TODO make relative to scene root
    public void MarkerDetected(GameObject image, Vector3 position, Quaternion rotation) {

        var alignedRotation = rotation * Quaternion.AngleAxis(0, Vector3.up) * Quaternion.Inverse(image.transform.rotation);
        var alignedPosition = position - alignedRotation * image.transform.position;
        // Debug.Log("Plinth Detect: Detected marker at: " + rotation.eulerAngles + " and:" + alignedRotation.eulerAngles);

        var angles = alignedRotation.eulerAngles;


        if (angles.x > 10 && angles.x < 360 - 10) return;
        if (angles.z > 10 && angles.z < 360 - 10) return;

        if (angles.y < 2 || angles.y > 360 - 2) {
            angles.y = 0;
        }



        _root.transform.position = alignedPosition;
        _root.transform.rotation = Quaternion.Euler(new Vector3(0, angles.y, 0));

        GetAlignment();
    }

    void OnGUI() {
        if (!Application.isEditor) {
            return;
        }
        if (GUI.Button(new Rect(0, 60, 120, 40), "Align Marker")) {
            DebugGetAlignment();
        }
    }

    void DebugGetAlignment() {
        if (!_aligned) {
            GetAlignment();
        }
    }
}