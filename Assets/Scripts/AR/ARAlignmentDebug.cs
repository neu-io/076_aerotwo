﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ARAlignmentDebug : MonoBehaviour, IPointerClickHandler {
    [Require] public GameObject _detectedImage;
    [Require] public GameObject _targetImage;
    [Require] public ARAlignment _alignment;
    public void OnPointerClick(PointerEventData eventData) {
        var trans = _detectedImage.transform;
        _alignment.MarkerDetected(_targetImage, trans.position, trans.rotation);
    }
}
