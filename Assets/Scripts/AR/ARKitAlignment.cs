﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class ARKitAlignment : MonoBehaviour {
    public static List<ARKitMarker> markers = new List<ARKitMarker>();
    [Require] public ARAlignment alignment;

    void OnEnable() {
        UnityARSessionNativeInterface.ARImageAnchorAddedEvent += MarkerDetected;
        UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent += MarkerDetected;
    }
    void OnDisable() {
        UnityARSessionNativeInterface.ARImageAnchorAddedEvent -= MarkerDetected;
        UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent -= MarkerDetected;
    }
    void MarkerDetected(ARImageAnchor image) {
        GameObject imageObject = null;
        foreach (var marker in markers) {
            if (image.referenceImageName == marker.image.imageName) {
                imageObject = marker.gameObject;
                break;
            }
        }
        if (imageObject == null)
            return;

        var position = UnityARMatrixOps.GetPosition(image.transform);
        var rotation = UnityARMatrixOps.GetRotation(image.transform);


        alignment.MarkerDetected(imageObject, position, rotation);
    }
}
