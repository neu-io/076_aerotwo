using UnityEngine;
using UnityEngine.XR.iOS;

public class ARKitMarker : MonoBehaviour {
    [Require] public ARReferenceImage image;
    void Awake() {
        ARKitAlignment.markers.Add(this);
    }
    void OnDestroy() {
        ARKitAlignment.markers.Remove(this);
    }
}