﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class ARKitPlayer : MonoBehaviour {
    GameObject _player {
        get {
            if (WorldSettingsScript.instance.player == null) return null;
            return WorldSettingsScript.instance.player.gameObject;
        }
    }

    UnityARSessionNativeInterface _session;
    void Start() {

        _session = UnityARSessionNativeInterface.GetARSessionNativeInterface();

    }
    void Update() {
        if (_player == null) return;
        Matrix4x4 matrix = _session.GetCameraPose();
        _player.transform.localPosition = UnityARMatrixOps.GetPosition(matrix);
        _player.transform.localRotation = UnityARMatrixOps.GetRotation(matrix);
    }
}
