﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetEverything : MonoBehaviour {
    public KeyCode key;
    void Update() {
        if (Input.GetKeyDown(key)) {
            var soundObjects = FindObjectsOfType<SoundObjectDelete>();
            Debug.Log("Cleanup");
            foreach (var soundObj in soundObjects) {
                WorldSettingsScript.instance.player.CmdDeleteSound(soundObj.deleteTarget);

            }
        }
    }
}
