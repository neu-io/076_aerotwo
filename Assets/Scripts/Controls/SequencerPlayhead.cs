﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioHelm;

public class SequencerPlayhead : MonoBehaviour {
    public static double position;

    [Require] public Sequencer sequencer;
    void Update() {
        position = sequencer.GetSequencerPosition() / 16;
        var y = (float)sequencer.GetSequencerPosition() / 16;
        y *= 360f;
        transform.localEulerAngles = new Vector3(0, y, 0);
    }
}
