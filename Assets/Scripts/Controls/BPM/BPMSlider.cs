﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BPMSlider : MonoBehaviour {
    AudioHelm.AudioHelmClock bpmClockRef;

    public float bpm;
    public Vector3 relativePosition {
        get {
            return WorldSettingsScript.instance.sceneRoot.worldToLocalMatrix.MultiplyPoint(transform.position);
        }
    }
    // Start is called before the first frame update
    void Start() {
        bpmClockRef = FindObjectOfType<AudioHelm.AudioHelmClock>();

    }
    void Update() {
        bpm = relativePosition.y * 100;
        bpmClockRef.bpm = bpm;

    }
}
