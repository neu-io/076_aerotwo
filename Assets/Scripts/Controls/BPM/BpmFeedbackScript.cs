﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BpmFeedbackScript : MonoBehaviour {
    Renderer _renderer;
    Vector3 lastPos;
    Color initialColor;

    public Vector3 relativePosition {
        get {
            return WorldSettingsScript.instance.sceneRoot.worldToLocalMatrix.MultiplyPoint(transform.position);
        }
    }

    // Start is called before the first frame update
    void Start() {
        _renderer = gameObject.GetComponent<Renderer>();
        lastPos = transform.position;
        initialColor = _renderer.material.color;
        _renderer.material.EnableKeyword("_EMISSION");
    }

    // Update is called once per frame
    void Update() {
        if (lastPos != relativePosition) {
            Color updatedColor = Color.red * (relativePosition.y / 2);
            _renderer.material.color = initialColor / (relativePosition.y * 2) + updatedColor;
            lastPos = relativePosition;
        }
    }
}
