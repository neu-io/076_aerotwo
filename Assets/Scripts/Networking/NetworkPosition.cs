﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class NetworkPosition : NetworkBehaviour {
    Vector3 lastPos;
    void Update() {
        if (lastPos != transform.localPosition) {
            SetDirtyBit(1u);
            lastPos = transform.localPosition;
        }
    }

    public override bool OnSerialize(NetworkWriter writer, bool initialState) {
        writer.Write(transform.localPosition);
        return true;
    }

    public override void OnDeserialize(NetworkReader reader, bool initialState) {
        transform.localPosition = reader.ReadVector3();
        lastPos = transform.localPosition;
        Debug.Log("Deserialising!");
    }
}
