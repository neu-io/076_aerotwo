﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioHelm;
using Mirror;

public class SyncClock : NetworkBehaviour {
    public float syncEverySec = 30f;
    public float syncDelay = 0.2f;
    [Require] public AudioHelmClock clock;


    float lastSync;
    void Start() {
        WorldSettingsScript.instance.clock = this;
    }
    void Update() {
        if (!NetworkServer.active) {
            return;
        }
        if (Time.time - lastSync > syncEverySec) {
            var time = NetworkTime.time + syncDelay;
            StartCoroutine(SyncTime(time));
            RpcSetSyncTime(time);
            lastSync = Time.time;
        }
    }

    public void Sync() {
        clock.Reset();
        RpcReset();
    }

    IEnumerator SyncTime(double time) {
        while (NetworkTime.time < time) yield return null;

        clock.Reset();
    }

    [ClientRpc]
    void RpcReset() {
        clock.Reset();
    }

    [ClientRpc]
    void RpcSetSyncTime(double time) {
        StartCoroutine(SyncTime(time));
    }
}
