﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class AutoStart : MonoBehaviour {
    void Start() {
#if UNITY_EDITOR
        GetComponent<NetworkManager>().StartHost();
#endif
    }
}
