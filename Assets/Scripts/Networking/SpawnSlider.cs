﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class SpawnSlider : NetworkBehaviour {
    [Require] public GameObject slider;
    public override void OnStartServer() {
        var obj = Instantiate(slider);
        obj.transform.SetParent(WorldSettingsScript.instance.sceneRoot, false); 
        NetworkServer.Spawn(obj);
    }
}
