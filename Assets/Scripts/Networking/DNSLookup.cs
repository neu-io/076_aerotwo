﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using Mirror;

public class DNSLookup : MonoBehaviour {
    [Require] public NetworkManager manager;

    public string url;
    void Start() {
        IPHostEntry hostInfo = Dns.GetHostEntry(url);
        manager.networkAddress = hostInfo.AddressList[0].ToString();
    }
}
