﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class SoundPlayer : NetworkBehaviour {

    void Start() {
        if (isLocalPlayer) {
            WorldSettingsScript.instance.player = this;
        }
    }

    public void SpawnSound(SoundType type, Vector3 position) {
        CmdSpawnSound(type, position);
    }

    [Command]
    public void CmdSpawnSound(SoundType type, Vector3 position) {
        WorldSettingsScript.instance.soundObjectManager.SpawnSound(type, position);
    }
    [Command]
    public void CmdMoveSound(GameObject soundObject, Vector3 position) {
        soundObject.transform.localPosition = position;
    }
    [Command]
    public void CmdDeleteSound(GameObject soundObject) {
        Destroy(soundObject);
    }
}
