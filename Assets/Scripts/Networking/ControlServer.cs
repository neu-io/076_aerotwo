﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ControlServer : MonoBehaviour {
    [Require] public NetworkManager networkManager;
    public void SetIP(string ipstring) {
        networkManager.networkAddress = ipstring;
    }
    public void StartServer() {
        networkManager.StopHost();
        networkManager.StartHost();
    }
    public void StartClient() {
        networkManager.StopHost();
        networkManager.StartClient();
    }
}
