﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Mirror;

public class DiscoverServer : NetworkDiscovery {
    Mirror.NetworkManager server;
    void Start() {
        Initialize();
        if (Mirror.NetworkServer.active) {
            StartAsServer();
        }
        else {
            StartAsClient();
        }
    }
    public override void OnReceivedBroadcast(string fromAddress, string data) {
        fromAddress = fromAddress.Replace("::ffff:", "");
        Mirror.NetworkManager.singleton.networkAddress = fromAddress;
    }
}
