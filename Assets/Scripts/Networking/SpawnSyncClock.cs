﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class SpawnSyncClock : NetworkBehaviour {
    [Require] public GameObject clock;
    public override void OnStartServer() {
        var obj = Instantiate(clock);
        obj.transform.parent = transform.parent;
        NetworkServer.Spawn(obj);
    }
}
