﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSoundObject : MonoBehaviour {
    public float debounce = 0.2f;
    float lastUpdate = -1;

    Vector3 lastPos;

    void Update() {
        var pos = transform.localPosition;
        if (pos != lastPos) {
            lastUpdate = Time.time;
        }
        lastPos = pos;

        if (lastUpdate == -1 || Time.time - lastUpdate < debounce) {
            return;
        }

        var newPos = transform.parent.localPosition + pos;

        WorldSettingsScript.instance.player.CmdMoveSound(transform.parent.gameObject, newPos);
        transform.parent.localPosition = newPos;

        transform.localPosition = Vector3.zero;
        lastPos = Vector3.zero;
        lastUpdate = -1;
    }
}
