﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARInteractable : MonoBehaviour {
    public static List<ARInteractable> interactables = new List<ARInteractable>();
    public void Spawn() {
        Instantiate(gameObject, Camera.main.transform.position + Camera.main.transform.forward * 0.15f, Quaternion.identity);
    }

    void Awake() {
        interactables.Add(this);
    }
    void OnDestroy() {
        interactables.Remove(this);
    }
}
