﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARInteractableMove : MonoBehaviour {
    List<TouchExtra> touches = new List<TouchExtra>();

    public Transform dragger;
    public bool disableX;
    public bool disableY;
    public bool disableZ;

    Matrix4x4 offset;
    Vector3 startPos;
    Vector3 startRot;

    public void Add(TouchExtra touch, Transform dragger) {
        touches.Add(touch);
        if (touches.Count != 1) {
            return;
        }
        this.dragger = dragger;
        offset = Matrix4x4.TRS(dragger.transform.position, dragger.transform.rotation, Vector3.one).inverse * Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);

        startPos = transform.position;
        startRot = transform.eulerAngles;
    }

    void Update() {
        ARInteractableManager.CheckTouches(touches);
        if (touches.Count < 1) {
            return;
        }

        var newOffset = CalcOffset();

        Matrix4x4 newTrans = Matrix4x4.TRS(dragger.transform.position, dragger.transform.rotation, Vector3.one) * newOffset;

        var posOffset = Utils.GetPosition(newTrans) - startPos;
        if (disableX) posOffset.x = 0;
        if (disableY) posOffset.y = 0;
        if (disableZ) posOffset.z = 0;
        var rotOffset = Utils.GetRotation(newTrans).eulerAngles - startRot;


        transform.position = startPos + posOffset;
    }
    Matrix4x4 CalcOffset() {
        //BUGFIX: Offset should be calculated from touch points and not object origin

        var touch = touches[0];

        var start = Utils.GetPosition(offset);

        Vector3[] corners = new Vector3[4];
        Camera.main.CalculateFrustumCorners(new Rect(0, 0, 1, 1), start.z, Camera.MonoOrStereoscopicEye.Mono, corners);

        var trans = corners[0];
        trans /= -0.5f;
        trans *= touch.norm;

        var orig = corners[0];
        orig /= -0.5f;
        orig *= touch.startNorm;

        var newPos = start + (trans - orig);

        return Matrix4x4.TRS(newPos, Utils.GetRotation(offset), Vector3.one);
    }
}
