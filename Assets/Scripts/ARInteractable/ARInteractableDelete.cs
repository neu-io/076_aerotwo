﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARInteractableDelete : MonoBehaviour {
    public void DeleteAll() {
        var objects = FindObjectsOfType<ARInteractable>();
        foreach (var obj in objects) {
            Destroy(obj.gameObject);
        }
    }
}
