﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARMoveManager : MonoBehaviour {
    void OnEnable() {
        ARInteractableManager.onTouchStart += onTouchStart;
    }
    void OnDisable() {
        ARInteractableManager.onTouchStart -= onTouchStart;
    }

    bool onTouchStart(TouchExtra touch, GameObject obj) {
        var move = obj.GetComponent<ARInteractableMove>();
        if (move == null) {
            move = obj.AddComponent<ARInteractableMove>();
        }
        move.Add(touch, Camera.main.transform);
        return false;
    }
}
