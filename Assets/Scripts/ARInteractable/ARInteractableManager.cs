﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class ARInteractableManager : MonoBehaviour {
    [SerializeField] bool multiTouchDebug = true;
    static bool _multiTouchDebug = true;

    Dictionary<int, TouchExtra> touches = new Dictionary<int, TouchExtra>();

    static BindingFlags flag = BindingFlags.Instance | BindingFlags.NonPublic;
    static Dictionary<string, FieldInfo> fields;

    public delegate bool TouchStart(TouchExtra touch, GameObject obj);
    public static TouchStart onTouchStart;

    public delegate bool TouchMiss(TouchExtra touch);
    public static TouchMiss onTouchMiss;

    public static int Count {
        get {
            return instance.touches.Count;
        }
    }
    static ARInteractableManager instance;

    void Start() {
        instance = this;
        _multiTouchDebug = multiTouchDebug;
        fields = new Dictionary<string, FieldInfo>();
        foreach (var f in typeof(Touch).GetFields(BindingFlags.Instance | BindingFlags.NonPublic)) {
            fields.Add(f.Name, f);
        }
    }

    void Update() {
        foreach (var touch in InputHelper.touches) {
            if (!touches.ContainsKey(touch.fingerId)) {
                touches.Add(touch.fingerId, new TouchExtra(touch));
            }
            var newtouch = touches[touch.fingerId];
            newtouch.touch = touch;

            if (touch.phase == TouchPhase.Ended) {
                touches.Remove(touch.fingerId);
            }

        }
        foreach (var touch in touches.Values) {
            ConnectTouch(touch);
        }
    }
    void ConnectTouch(TouchExtra touch) {
        if (touch.used) {
            return;
        }

        if (Utils.IsOverGUI(touch.pos)) {
            touch.used = true;
            return;
        }

        RaycastHit hit;
        if (!Physics.Raycast(Camera.main.ScreenPointToRay(touch.pos), out hit, 30f)) {
            if (onTouchMiss != null) onTouchMiss(touch);
            touch.used = true;
            return;
        }
        var obj = hit.transform.gameObject;
        var interactables = obj.GetComponentsInParent<ARInteractable>();
        if (interactables.Length == 0) {
            return;
        }
        obj = interactables[interactables.Length - 1].gameObject;

        touch.startLoc = hit.point;

        if (onTouchStart != null) {
            foreach (TouchStart touchStart in onTouchStart.GetInvocationList()) {
                var used = touchStart(touch, obj);
                if (used) {
                    touch.used = true;
                }
            }
        }
        if (!touch.used && onTouchMiss != null) onTouchMiss(touch);
        touch.used = true;
    }
    public static void CheckTouches(List<TouchExtra> touches) {
        foreach (var touch in touches.ToArray()) {
            if (touch.touch.phase == TouchPhase.Ended) {
#if UNITY_IOS && UNITY_EDITOR
                if (_multiTouchDebug) continue;
#endif
                touches.Remove(touch);
            }
        }
    }

    void OnApplicationFocus(bool focusStatus) {
        if (focusStatus) {
            return;
        }
        foreach (var touch in touches.Values) {
            touch.touch.phase = TouchPhase.Ended;
        }
    }
}
public class TouchExtra {
    public Touch touch;
    public bool used;
    public Vector2 startPos;
    public Vector3 startLoc;

    public Vector2 startNorm;
    public float dist {
        get {
            return Vector2.Distance(startPos, pos);
        }
    }
    public Vector2 norm {
        get {
            return Normalise(pos);
        }
    }
    public Vector2 pos {
        get {
            return touch.position;
        }
    }
    public TouchExtra(Touch touch) {
        this.touch = touch;
        this.startPos = pos;
        this.startNorm = Normalise(pos);
    }
    public static Vector2 Normalise(Vector2 pos) {
        var screen = new Vector2(Screen.width, Screen.height);
        pos = (pos - screen / 2) / screen;
        return pos;
    }
}