﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class SetLineColorByDistance : MonoBehaviour
{
    Vector3 interactionSpace;
    public float distanceFromCam = 1.5f;
    public float minAlpha = 0.2f;
    
    public GameObject cameraRef;
    [Require]
    public LineRenderer rend;
    
    
       
    void Start()
    {
        if (cameraRef == null) cameraRef = Camera.main.gameObject;
        if (rend == null) rend = GetComponent<LineRenderer>();
    }

    void Update()
    {
        interactionSpace = cameraRef.transform.position + cameraRef.transform.forward * distanceFromCam;
        
        float distanceToInteractionSpace = Vector3.Distance(interactionSpace, transform.position);

        rend.startColor = new Color(0.98f, 0.11f, 0.81f, Mathf.Clamp(1 - distanceToInteractionSpace / 2, minAlpha, 1));
        rend.endColor = new Color(0.98f, 0.11f, 0.81f, Mathf.Clamp(1 - distanceToInteractionSpace / 2, minAlpha, 1));
        
    }
}
