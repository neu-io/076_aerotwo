﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NEEEU;

public class PitchFeedbackScript : MonoBehaviour
{
    
    Vector3 lastPos;
    Vector3 initialScale;
    // Start is called before the first frame update
    void Start()
    {
        initialScale = transform.localScale;
        SetScaleByPitch();
    }

    // Update is called once per frame
    void Update()
    {
        if(lastPos != transform.position)
        {
            SetScaleByPitch();
        }
    }

    void SetScaleByPitch()
    {
        float mappedY = NeeeuUtils.Map(0f, 2f, 0.5f, 1.5f, transform.position.y);
        Vector3 scaleChange = new Vector3(initialScale.x / mappedY, initialScale.y, initialScale.z / mappedY);
        transform.localScale = scaleChange;
        
        lastPos = transform.position;
    }
}
