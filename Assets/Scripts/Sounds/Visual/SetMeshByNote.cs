﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioHelm;

public class SetMeshByNote : MonoBehaviour {
    public List<ShapeAndRange> _shapeList = new List<ShapeAndRange>();

    [Require] public PositionToNote posNoteRef;

    int lastNote;

    void Update() {
        if (posNoteRef.note.note == lastNote) return;
        foreach (ShapeAndRange shapeStruct in _shapeList) {
            if (posNoteRef.note.note >= shapeStruct.rangeMin && posNoteRef.note.note <= shapeStruct.rangeMax) {
                shapeStruct.soundShape.SetActive(true);
            }
            else {
                shapeStruct.soundShape.SetActive(false);
            }
        }
        lastNote = posNoteRef.note.note;
    }
}

[System.Serializable]
public struct ShapeAndRange {
    public GameObject soundShape;
    public int rangeMin;
    public int rangeMax;
}