﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundObjectFeedback : MonoBehaviour {
    [Require] public PositionToNote position;
    public double diff;

    public virtual void StartFeeback() {
        transform.localScale = Vector3.one * 1.5f;
    }
    public virtual void StopFeeback() {
        transform.localScale = Vector3.one;
    }

    bool triggerd;

    void Update() {
        diff = SequencerPlayhead.position - position.normalisedLoopPosition;
        var active = diff < 0.05f && diff >= -0.05f;

        if (active && !triggerd) StartFeeback();
        if (!active && triggerd) StopFeeback();

        triggerd = active;
    }
}
