﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
[ExecuteInEditMode]
public class SoundLineBetween : MonoBehaviour {
    [Require] public GameObject visual;
    [Require] public GameObject shadow;

    LineRenderer renderer;
    void Start() {
        renderer = GetComponent<LineRenderer>();
    }

    void Update() {
        var points = new List<Vector3>();
        points.Add(visual.transform.localPosition);
        points.Add(shadow.transform.localPosition);

        renderer.positionCount = 2;
        renderer.SetPositions(points.ToArray());
    }
}
