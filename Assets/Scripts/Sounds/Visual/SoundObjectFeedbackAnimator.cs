﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundObjectFeedbackAnimator : SoundObjectFeedback {
    [Require] public Animator animator;
    public override void StartFeeback() {
        animator.SetTrigger("NoteFeedbackTrigger");
    }
    public override void StopFeeback() {
    }
}
