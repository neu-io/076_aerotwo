﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleBehaviour : MonoBehaviour
{
    public float spriteOffsetXZ = 0.15f;
    public float spriteOffsetY = 0.1f;
    public float minAlpha = 0.05f;
    public float maxAlpha = 0.5f;
    public float distanceFromCam = 1;
    public float fadeIntensity = 1;

    [Require]
    public SpriteRenderer spriteRend;

    public GameObject cameraRef;
    
    void Start()
    {
        if (cameraRef == null) cameraRef = Camera.main.gameObject;
    }

    void Update()
    {
        transform.forward = -cameraRef.transform.forward;
        transform.position = transform.parent.transform.position + transform.forward * spriteOffsetXZ + transform.up * spriteOffsetY;

        Vector3 interactionSpace = cameraRef.transform.position + cameraRef.transform.forward * distanceFromCam;

        float distanceToInteractionSpace = Vector3.Distance(interactionSpace, transform.position);

        Color c = spriteRend.color;

        spriteRend.color = new Color(c.r, c.g, c.b, Mathf.Clamp(1 - distanceToInteractionSpace / (2 * fadeIntensity), minAlpha, maxAlpha));
        spriteRend.color = new Color(c.r, c.g, c.b, Mathf.Clamp(1 - distanceToInteractionSpace / (2 * fadeIntensity), minAlpha, maxAlpha));
        
    }
}
