﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundObjectFeedbackParticleSystem : SoundObjectFeedback {

    [Require] public ParticleSystem pSystem;
    public int EmitAccount = 200;
    public bool Burst = true;

    override public void StartFeeback() {
        var emitParams = new ParticleSystem.EmitParams();
        if( Burst )
        {
            pSystem.Emit( emitParams, EmitAccount );
        } else
        {
            pSystem.Play();
        }

    }

    override public void StopFeeback() {
    }

}
