﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SoundObjectSpawner : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    public SoundType type;
    public float delay = 1f;
    public float distance = 0.2f;

    bool spawning;
    float lastSpawn;

    public void OnPointerDown(PointerEventData eventData) {
        spawning = true;
    }

    public void OnPointerUp(PointerEventData eventData) {
        spawning = false;
    }

    void Update() {
        if (!spawning) {
            return;
        }
        if (Time.time - lastSpawn < delay) {
            return;
        }

        var camTransform = Camera.main.transform;
        var position = camTransform.position;
        position += camTransform.forward * distance;
        position = WorldSettingsScript.instance.sceneRoot.worldToLocalMatrix.MultiplyPoint(position);
        // position -= WorldSettingsScript.instance.sceneRoot.position;

        WorldSettingsScript.instance.player.SpawnSound(type, position);

        lastSpawn = Time.time;
    }
}
