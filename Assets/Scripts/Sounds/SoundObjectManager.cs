﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class SoundObjectManager : MonoBehaviour {
    public List<SoundObjectPrefab> prefabs = new List<SoundObjectPrefab>();

    public void SpawnSound(SoundType type, Vector3 position) {
        var prefab = GetPrefab(type);

        var gameObject = Instantiate(prefab, position, Quaternion.identity);

        NetworkServer.Spawn(gameObject);

    }
    public GameObject GetPrefab(SoundType type) {
        foreach (var prefab in prefabs) {
            if (prefab.type == type) {
                return prefab.prefab;
            }
        }
        Debug.LogError("Spawning impossible type");
        return null;
    }
}

[Serializable]
public class SoundObjectPrefab {
    public SoundType type;
    public GameObject prefab;

}