﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundObjectMover : MonoBehaviour {
    public bool disableX;
    public bool disableY;
    public bool disableZ;
    void Update() {
        var pos = transform.localPosition;
        if (disableX) pos.x = 0;
        if (disableY) pos.y = 0;
        if (disableZ) pos.z = 0;
        if (pos != Vector3.zero) {
            var parentpos = transform.parent.localPosition;
            parentpos += pos;
            transform.parent.localPosition = parentpos;
        }
        transform.localPosition = transform.localPosition - pos;
    }
}
