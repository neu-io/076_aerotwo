﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SoundObjectBase : MonoBehaviour {
    public Vector3 rootPosition {
        get {
            return WorldSettingsScript.instance.sceneRoot.worldToLocalMatrix.MultiplyPoint(transform.position);
        }
    }

    void Update() {
        var pos = rootPosition;
        pos.z = 0;
        pos.x = 0;
        transform.localPosition = transform.localPosition - pos;
    }
}
