﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SoundObjectDelete : MonoBehaviour {
    [Require] public GameObject deleteTarget;
    public RectTransform uiTarget;
    public GraphicRaycaster raycaster;

    List<TouchExtra> touches = new List<TouchExtra>();

    public void Add(TouchExtra touch) {
        touches.Add(touch);
    }

    void Update() {
        var onDelete = new List<TouchExtra>();

        foreach (var touch in touches) {
            var pointerEventData = new PointerEventData(EventSystem.current);
            pointerEventData.position = touch.pos;

            List<RaycastResult> results = new List<RaycastResult>();

            raycaster.Raycast(pointerEventData, results);

            foreach (RaycastResult result in results) {
                if (result.gameObject.GetComponent<RectTransform>() == uiTarget) {
                    onDelete.Add(touch);
                    break;
                }
            }
        }

        ARInteractableManager.CheckTouches(touches);

        if (touches.Count != 0 || onDelete.Count == 0) return;

        WorldSettingsScript.instance.player.CmdDeleteSound(deleteTarget);
    }
}
