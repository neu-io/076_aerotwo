﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioHelm;

public class SoundObject : MonoBehaviour {
    public SoundType type;
}
public enum SoundType {
    None,
    Drum,
    Bass,
    Lead,
    DrumKit2,
    Bass2,
    Bass3,
    Lead2,
    Lead3
}