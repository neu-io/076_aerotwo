﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using NatCorder;
using NatCorder.Clocks;
using NatCorder.Inputs;
using System;

public class UIRecorder : MonoBehaviour, IPointerClickHandler {
    public float maxRecordTime;

    public event Action onRecordingStart;
    public event Action onRecordingEnd;
    public event Action<string> onRecordingSaved;

    public

    static float recordingWidth = 1080f;
    static float recordingHeight = 1920f;

    MP4Recorder _videoRecorder;
    IClock _recordingClock;
    AudioInput _audioInput;

    CameraInput _cameraInput;

    public float progress {
        get {
            return Mathf.Clamp((Time.time - _recordingStart) / maxRecordTime, 0, 1);
        }
    }
    bool _recording;
    public bool recording {
        get {
            return _recording;
        }
    }
    float _recordingStart;

    public void OnPointerClick(PointerEventData eventData) {
        if (!_recording) {
            StartCoroutine(StartRecording());
        }
        else {
            _recording = false;
        }
    }

    public static Vector2Int RecordingSize() {
        var width = recordingWidth;
        var height = recordingHeight;

        var ratio = ((float)Screen.width) / Screen.height;
        var targetRatio = width / height;
        if (ratio > targetRatio) {
            height = recordingWidth / ratio;
        }
        else {
            width = recordingHeight * ratio;
        }
        return new Vector2Int((int)width, (int)height);
    }

    IEnumerator StartRecording() {
        if (onRecordingStart != null) onRecordingStart.Invoke();

        var size = RecordingSize();

        _recordingClock = new RealtimeClock();
        _videoRecorder = new MP4Recorder(
            size.x,
            size.y,
            60,
            44100,
            2,
            OnVideoRecorded
        );
        _cameraInput = new CameraInput(_videoRecorder, _recordingClock, Camera.main);
        _audioInput = new AudioInput(_videoRecorder, _recordingClock, Camera.main.GetComponent<AudioListener>());

        _recording = true;
        _recordingStart = Time.time;


        while (_recording && progress != 1) yield return null;

        _cameraInput.Dispose();
        _videoRecorder.Dispose();
        _audioInput.Dispose();

        if (onRecordingEnd != null) onRecordingEnd.Invoke();
    }


    void OnDisable() {
        _recording = false;
    }

    void OnVideoRecorded(string path) {
        SaveVideo.Save(path);
        if (onRecordingSaved != null) onRecordingSaved(path);
    }


}
