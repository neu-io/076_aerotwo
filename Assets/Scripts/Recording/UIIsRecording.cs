using UnityEngine;

[RequireComponent(typeof(UIFade))]
public class UIIsRecording : MonoBehaviour {
    [Require] public UIRecorder recorder;
    [Require] public UIFade fade;
    void Awake() {
        recorder.onRecordingStart += () =>
        {
            fade.target = 1;
        };
        recorder.onRecordingEnd += () =>
        {
            fade.target = 0;
        };
    }
}