﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OpenGallery : MonoBehaviour, IPointerClickHandler {
    public void OnPointerClick(PointerEventData eventData) {

#if UNITY_IOS
        Application.OpenURL("photos-redirect://");

#else

        Application.OpenURL("content://media/internal/images/media");
#endif

    }
}
