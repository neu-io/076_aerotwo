﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Runtime.InteropServices;

public class SaveVideo {
    public static void Save(string url) {
        var perm = NativeGallery.RequestPermission();
        if (perm == NativeGallery.Permission.Denied) {
            // TODO make a UI
            NativeGallery.OpenSettings();
        }


        NativeGallery.SaveVideoToGallery(url, "OrBeat", "OrBeatVideo{0}.mp4");


    }
}
