﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIShareButton : MonoBehaviour, IPointerClickHandler {
    public string url;
    public void OnPointerClick(PointerEventData eventData) {
        new NativeShare().AddFile(url).SetSubject("OrBeat Video").Share();
    }
}
