﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[RequireComponent(typeof(UISelectorElement))]
public class UIRecordingReview : MonoBehaviour {
    [Require] public UISelector selector;
    [Require] public UIRecorder recorder;
    [Require] public VideoPlayer player;
    [Require] public UIShareButton share;

    void Awake() {
        recorder.onRecordingSaved += VideoRecorded;
    }
    void VideoRecorded(string url) {
        selector.SetElement(gameObject);

        player.url = url;
        share.url = url;
    }
}
