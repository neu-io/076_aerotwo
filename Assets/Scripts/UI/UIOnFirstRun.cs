﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UIOnFirstRun : MonoBehaviour {
    public bool reset;
    public string prefName;

    public UnityEvent onFirstRun;

    void Awake() {
        var current = PlayerPrefs.GetInt(prefName, 0);
#if UNITY_EDITOR
        if (reset) {
            current = 0;
        }
#endif
        if (current == 0) {
            current++;
            onFirstRun.Invoke();
            PlayerPrefs.SetInt(prefName, current);
        }
    }
}
