﻿        using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAndRotate2DArrowTowardsPole : MonoBehaviour {

    private Camera cameraMain;
    [Require] public GameObject centerPole;

    private float screenWidth;
    private float screenHeight;

    private Vector3 destPos;
    private Vector3 poleCenter;

    private void Start() {
        cameraMain = Camera.main;
        screenWidth = GetComponent<RectTransform>().rect.width;
        screenHeight= GetComponent<RectTransform>().rect.height;
    }

    void Update() {

        poleCenter = cameraMain.WorldToScreenPoint(centerPole.transform.position);

        Vector3 targetDir = poleCenter - transform.position;
        float angle = Vector3.Angle(Vector3.up, targetDir);

        transform.up = targetDir;

        destPos = poleCenter;
        destPos.x = Mathf.Clamp(destPos.x, 0f+screenWidth/2, Screen.width-screenWidth/2);
        destPos.y = Mathf.Clamp(destPos.y, 0f+screenHeight/2, Screen.height-screenHeight/2);

        transform.position = Vector3.Lerp(transform.position, destPos, Time.deltaTime * 1.2f);
    }
}

