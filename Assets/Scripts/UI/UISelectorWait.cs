﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISelectorWait : MonoBehaviour {
    [Require] public UISelector selector;
    [Require] public GameObject element;
    public float waitTime;

    UISelectorElement intf {
        get {
            var ret = element.GetComponent(typeof(UISelectorElement)) as UISelectorElement;
            if (ret == null) {
                Debug.LogError("Element has no component wiht UISelectorelement");
            }
            return ret;
        }
    }
    void OnEnable() {
        StartCoroutine(Wait());
    }

    IEnumerator Wait() {
#if !UNITY_EDITOR
        yield return new WaitForSeconds(waitTime);
#else
        yield return null;
#endif
        selector.SetElement(intf);
    }
}
