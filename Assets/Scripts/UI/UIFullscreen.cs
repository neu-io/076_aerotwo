﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFullscreen : MonoBehaviour {
    RectTransform rectTransform;

    Rect lastSafeArea;
    void Awake() {
        rectTransform = GetComponent<RectTransform>();
    }
    void Update() {
        var fullScreen = new Rect(0, 0, Screen.width, Screen.height);
        if (lastSafeArea == fullScreen) return;

        var canvas = rectTransform.root.GetComponent<Canvas>();


        var anchorMin = fullScreen.position;
        var anchorMax = fullScreen.position + fullScreen.size;
        anchorMin.x /= canvas.pixelRect.width;
        anchorMin.y /= canvas.pixelRect.height;
        anchorMax.x /= canvas.pixelRect.width;
        anchorMax.y /= canvas.pixelRect.height;

        rectTransform.anchorMin = anchorMin;
        rectTransform.anchorMax = anchorMax;

        lastSafeArea = fullScreen;
    }
}
