﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ShowButtonOnConnect : MonoBehaviour
{
    public GameObject nextButton;
    bool executed = false;


    // Update is called once per frame
    void Update()
    {
        if (NetworkClient.singleton == null) return;

        if (NetworkClient.singleton.isConnected)
        {
            if (!executed)
            {
                nextButton.SetActive(true);
                executed = true;
            }
        }
    }
}
