﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class StreamVideo : MonoBehaviour
{
    [Require] public RawImage rawImage;
    [Require] public VideoPlayer videoPlayer;


    void Start()
    {
        var renderTexture = new RenderTexture((int)videoPlayer.clip.width, (int)videoPlayer.clip.height, 32);
        videoPlayer.targetTexture = renderTexture;
       rawImage.texture = renderTexture;

       
    }
}
