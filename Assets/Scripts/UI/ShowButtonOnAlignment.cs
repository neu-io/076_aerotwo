﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowButtonOnAlignment : MonoBehaviour
{
    [Require]
    public ARAlignment alignRef;
    [Require]
    public GameObject nextButton;

    bool executed = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (alignRef.aligned)
        {
            if (!executed)
            {
                nextButton.SetActive(true);
                executed = true;
            }
        }
    }
}
