﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ARDeleteManager : MonoBehaviour {
    List<TouchExtra> touches = new List<TouchExtra>();
    [Require] public RectTransform target;
    [Require] public GraphicRaycaster raycaster;
    [Require] public UISelector selector;
    [Require] public GameObject normalInterface;
    [Require] public GameObject deleteInterface;
    void OnEnable() {
        ARInteractableManager.onTouchStart += onTouchStart;
    }
    void OnDisable() {
        ARInteractableManager.onTouchStart -= onTouchStart;
    }
    bool onTouchStart(TouchExtra touch, GameObject obj) {
        var del = obj.GetComponent<SoundObjectDelete>();
        if (del == null) {
            return false;
        }
        del.uiTarget = target;
        del.raycaster = raycaster;
        del.Add(touch);
        touches.Add(touch);
        selector.SetElement(deleteInterface);
        return false;
    }
    void Update() {
        var touchesCount = touches.Count;
        ARInteractableManager.CheckTouches(touches);
        if (touchesCount == touches.Count) return;
        if (touches.Count != 0) return;

        selector.SetElement(normalInterface);
    }

}
