﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitPosition : MonoBehaviour {

    public Vector3 min;
    public Vector3 max;

    Vector3 position {
        get {
            return WorldSettingsScript.instance.sceneRoot.worldToLocalMatrix.MultiplyPoint(transform.position);
        }
        set {
            transform.position = WorldSettingsScript.instance.sceneRoot.localToWorldMatrix.MultiplyPoint(value);
        }
    }

    public Vector3 lastPos;
    void Update() {
        if (lastPos == transform.localPosition) {
            return;
        }

        var pos = position;
        if (max.x != 0 || min.x != 0) {
            pos.x = Mathf.Min(max.x, pos.x);
            pos.x = Mathf.Max(min.x, pos.x);
        }

        if (max.y != 0 || min.y != 0) {
            pos.y = Mathf.Min(max.y, pos.y);
            pos.y = Mathf.Max(min.y, pos.y);
        }

        if (max.z != 0 || min.z != 0) {
            pos.z = Mathf.Min(max.z, pos.z);
            pos.z = Mathf.Max(min.z, pos.z);
        }

        position = pos;
        lastPos = transform.localPosition;
    }
}
