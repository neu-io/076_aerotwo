﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTint : MonoBehaviour
{
  
    public float range = .40f;
    public float fadeLength = .20f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      
        var relativePosition = Camera.main.transform.position - transform.position; 
        var dot = Vector3.Dot(relativePosition.normalized, Camera.main.transform.forward);

        //normalise
        var angleNorm = dot = dot * .5f + .5f;

        range = Mathf.Clamp01(range);

        var tint = SmoothStep( range - fadeLength/2, range +fadeLength/2, angleNorm);

        // Unity's smoothstep is weird - not like in HLSL
        //var tint = Mathf.SmoothStep(range, range + fadeLength, angleNorm);

        GetComponent<Renderer>().material.color = new Color(tint, tint, tint);

       
    }

    float SmoothStep(float edge0, float edge1, float x)
    {

        var t = Mathf.Clamp01((x - edge0) / (edge1 - edge0));
        return t * t * (3.0f - 2.0f * t);

    }


}
