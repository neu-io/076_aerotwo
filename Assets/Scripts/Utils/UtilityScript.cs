﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NEEEU
{
    public class NeeeuUtils
    {
        public static float Map(float inMin, float inMax, float outMin, float outMax, float value, bool clamp = true)
        {
            float output = outMin + (outMax - outMin) * ((value - inMin) / (inMax - inMin));

            if (clamp)
            {
                if (output <= outMin) output = outMin;
                else if (output >= outMax) output = outMax;
            }

            return output;
        }
    }

}
