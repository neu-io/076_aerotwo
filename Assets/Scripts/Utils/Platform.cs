﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {
    public PlatformType platform;
    static Dictionary<PlatformType, List<RuntimePlatform>> platforms = new Dictionary<PlatformType, List<RuntimePlatform>>()
    {
        {PlatformType.iOS, new List<RuntimePlatform>{ RuntimePlatform.IPhonePlayer, RuntimePlatform.OSXEditor
} },
        {PlatformType.MacOS, new List<RuntimePlatform>{ RuntimePlatform.OSXPlayer, RuntimePlatform.OSXEditor } },
        {PlatformType.Android, new List<RuntimePlatform>{ RuntimePlatform.Android, RuntimePlatform.WindowsEditor } },
        {PlatformType.Windows, new List<RuntimePlatform>{ RuntimePlatform.WindowsPlayer, RuntimePlatform.WindowsEditor } },
    };
    void OnEnable() {
        if (!platforms[platform].Contains(Application.platform)) {
            gameObject.SetActive(false);
        }
    }

}
public enum PlatformType {
    iOS,
    Android,
    MacOS,
    Windows
}