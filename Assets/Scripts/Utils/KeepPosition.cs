﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepPosition : MonoBehaviour {
    public Vector3 position;
    void Update() {
        transform.position = position;
    }
}
