﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NEEEU;

public class BPMSliderRotation : MonoBehaviour
{

    Vector3 localRot;
    float multiplier = 4;

    private void Start()
    {
        localRot = transform.localRotation.eulerAngles;
    }

    void Update()
    {
        localRot.y = NeeeuUtils.Map(-1.0f, 1.0f, 360 * multiplier, -360 * multiplier, transform.position.y, false );
        transform.localRotation = Quaternion.Euler( localRot );
    }
}
