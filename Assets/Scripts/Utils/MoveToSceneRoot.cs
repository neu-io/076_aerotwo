﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToSceneRoot : MonoBehaviour {
    void Start() {
        transform.SetParent(WorldSettingsScript.instance.sceneRoot, false);
    }
}
