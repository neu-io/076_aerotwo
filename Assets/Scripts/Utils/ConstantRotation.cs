﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantRotation : MonoBehaviour
{
    public float Speed = 10.0f;
    public Vector3 RotationAngles = Vector3.up;

    void Start()
    {
        Speed = Random.Range( Speed - 20, Speed + 20 );
    }
    

    void Update()
    {
        float delta = Time.deltaTime;
        var deltaRotation = Quaternion.AngleAxis( Speed * delta, RotationAngles );
        transform.localRotation = deltaRotation * transform.localRotation;

    }
}
