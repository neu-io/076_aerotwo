﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockOrientation : MonoBehaviour {
    public ScreenOrientation orientation;

    void OnEnable() {
        Screen.orientation = orientation;
    }
    void OnDisable() {
        Screen.orientation = ScreenOrientation.AutoRotation;
    }
}
