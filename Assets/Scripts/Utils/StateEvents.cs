﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StateEvents : MonoBehaviour {
    public UnityEvent enabled;
    public UnityEvent disabled;
    void OnEnable() {
        enabled.Invoke();
    }
    void OnDisable() {
        disabled.Invoke();
    }
}
