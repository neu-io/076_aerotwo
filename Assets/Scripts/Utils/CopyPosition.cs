﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyPosition : MonoBehaviour {
    void Update() {
        var trans = Camera.main.transform;

        transform.position = trans.position;
        var pos = transform.localPosition;
        pos.y = 0;
        transform.localPosition = pos;
    }
}
