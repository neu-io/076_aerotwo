﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ARNetworkManager : NetworkManager {
    public GameObject cube;
    public bool isClient;
    // Start is called before the first frame update
    void Start() {
        if (isClient) {
            StartClient();
        }
        else {
            StartHost();

            var cubeInst = Instantiate(cube);
            NetworkServer.Spawn(cubeInst);
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
