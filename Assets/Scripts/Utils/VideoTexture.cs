﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class VideoTexture : MonoBehaviour {
    [Require] public RawImage image;
    [Require] public VideoPlayer player;
    ScreenOrientation lastOrientation = ScreenOrientation.AutoRotation;
    void Update() {
        if (lastOrientation != Screen.orientation) {
            var rt = new RenderTexture(Screen.width, Screen.height, 32);
            image.texture = rt;
            player.targetTexture = rt;
            lastOrientation = Screen.orientation;
        }
    }
}
