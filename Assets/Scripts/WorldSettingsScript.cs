﻿using UnityEngine;
using System.Collections.Generic;
using Mirror;

public class WorldSettingsScript : MonoBehaviour {


    [Require] public SoundObjectManager soundObjectManager;
    [Require] public SoundSequencerManager soundSequencerManager;
    [Require] public NetworkManager networkManager;
    [Require] public Transform sceneRoot;
    public SoundPlayer player;
    public SyncClock clock;
    public GameObject cameraRef;

    public static WorldSettingsScript instance {
        get {
            return _instance;
        }
    }
    static WorldSettingsScript _instance;

    WorldSettingsScript() {
        if (_instance == null) {
            _instance = this;
        }
        else {
            Debug.LogError("Only one worldsettings script can be active at one time");

            Destroy(this);

        }
    }
}
