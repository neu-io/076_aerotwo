﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioHelm;

public class SoundSequencer : MonoBehaviour {
    public SoundType type;
    [HideInInspector] public List<int> noteScale;
    [Require] public Sequencer sequencer;

    public static Dictionary<SoundType, List<int>> noteScales = new Dictionary<SoundType, List<int>>(){
        {
            SoundType.Lead, new List<int>{
                48, 50, 52, 55, 57, 60, 62, 64, 67, 69, 72, 74, 76, 79, 81, 84
            }
        },
        {
            SoundType.Lead2, new List<int>{
                48, 50, 52, 55, 57, 60, 62, 64, 67, 69, 72, 74, 76, 79, 81, 84
            }
        },
        {
            SoundType.Lead3, new List<int>{
                48, 50, 52, 55, 57, 60, 62, 64, 67, 69, 72, 74, 76, 79, 81, 84
            }
        },
        {
            SoundType.None, new List<int>{
                0, 2, 4, 7, 9, 12, 14, 16, 19, 21, 24, 26, 28, 31, 33, 36, 38, 40, 43, 45, 48, 50, 52, 55, 57, 60, 62, 64, 67, 69, 72, 74, 76, 79, 81, 84
            }
        },
        {
            SoundType.Bass, new List<int>{
                60, 62, 64, 67, 69, 72, 74, 76, 79, 81, 84
            }
        },
        {
            SoundType.Bass2, new List<int>{
                60, 62, 64, 67, 69, 72, 74, 76, 79, 81, 84
            }
        },
        {
            SoundType.Bass3, new List<int>{
                60, 62, 64, 67, 69, 72, 74, 76, 79, 81, 84
            }
        },
        {
            SoundType.Drum, new List<int>{
                60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71
            }
        },
        {
            SoundType.DrumKit2, new List<int>{
                60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71
            }
        }
    };

    void Awake() {
        var sequencers = WorldSettingsScript.instance.soundSequencerManager.sequencers;
        if (sequencers.ContainsKey(type)) {
            Debug.LogError("Type sequencer is already defined");
        }
        sequencers.Add(type, this);

        if (!noteScales.ContainsKey(type)) {
            Debug.LogError("Type doesn't have note scales defined");
        }
        noteScale = noteScales[type];
    }
}
