﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using NEEEU;
using Mirror;
using AudioHelm;
using System;

public class PositionToNote : MonoBehaviour {
    static Dictionary<SoundType, Action> onPositionChanged = new Dictionary<SoundType, Action>();
    [Require] public SoundObject soundObject;
    public Vector3 relativePos;
    public Sequencer sequencer;
    public Note note;

    public float normalisedLoopPosition;

    SoundType type {
        get {
            return soundObject.type;
        }
    }
    private List<int> noteScale;
    public int sequencePos;

    public Vector3 relativePosition {
        get {
            return WorldSettingsScript.instance.sceneRoot.worldToLocalMatrix.MultiplyPoint(transform.position);
        }
    }

    public int lowestNote = 0;
    public int highestNote = 127;


    Vector3 lastPlace;


    void Start() {
        var soundSequencer = WorldSettingsScript.instance.soundSequencerManager.sequencers[type];
        noteScale = soundSequencer.noteScale;
        if (noteScale != null) {
            lowestNote = noteScale[0];
            highestNote = noteScale.Last();
        }
        sequencer = soundSequencer.sequencer;

        if (!onPositionChanged.ContainsKey(type)) {
            onPositionChanged.Add(type, new Action(PlaceSoundObj));
        }
        else {
            onPositionChanged[type] += PlaceSoundObj;
        }
    }

    void Update() {
        var pos = relativePosition;
        relativePos = pos;
        if (lastPlace != pos) {
            onPositionChanged[type].Invoke();
            lastPlace = pos;
        }
    }

    public void PlaceSoundObj() {
        normalisedLoopPosition = Mathf.Atan2(relativePosition.z, relativePosition.x) / Mathf.PI / 2.0f;
        if (normalisedLoopPosition < 0) normalisedLoopPosition += 1;
        normalisedLoopPosition = 1 - normalisedLoopPosition;
        sequencePos = Mathf.RoundToInt(normalisedLoopPosition * sequencer.length);

        var noteIndex = Mathf.RoundToInt(NeeeuUtils.Map(0f, 2f, lowestNote, highestNote, relativePosition.y));

        noteIndex = checkIfInScale(noteIndex);

        sequencer.RemoveNote(note);
        note = sequencer.AddNote(noteIndex, sequencePos, sequencePos % sequencer.length + 1);
    }

    void OnDestroy() {
        sequencer.RemoveNote(note);
        onPositionChanged[type] -= PlaceSoundObj;
    }

    public int checkIfInScale(int note) {
        if (noteScale == null) {
            return note;
        }
        return closestInScale(note);
    }

    public int closestInScale(int note) {
        return noteScale.Aggregate((x, y) => Mathf.Abs(x - note) < Mathf.Abs(y - note) ? x : y);
    }
}
