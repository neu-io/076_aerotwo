﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSequencerManager : MonoBehaviour {
    public Dictionary<SoundType, SoundSequencer> sequencers = new Dictionary<SoundType, SoundSequencer>();
}
